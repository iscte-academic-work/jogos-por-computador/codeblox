﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class CompanionMessages
{

    public const string UNUSED_BLOXES = "Ainda não usaste todos os blocos obrigatórios e/ou opcionais. Experimenta aplicar um deles.";
    public const string TRY_LEVEL_VARIANT = "Se estiveres com dificuldades, tenta começar de novo com um problema diferente.";
    public const string TRY_SOLUTION = "Podes sempre tentar obter uma possível solução e mudar à tua maneira.";
    public const string CHECK_FOR_BLOX_HELP = "Clica em Help, e de seguida selecciona um bloco para obteres ajuda.";
    public const string NO_HINT = "Sem pistas disponíveis por enquanto.";

    public const string CODE_SUGGESTION = "Ao todo o personagem deverá realizar {0} passos. A realização dos passos {1} a {2} poderá ser programada com os seguintes blocos:\n{3}";

    public const string CODE_SUGGESTION_BLOX_AMOUNT_TEMPLATE = "\t{0}x{1}\n";

    public static List<string> RANDOM_HINTS = new List<string>()
    {
        CHECK_FOR_BLOX_HELP,
        "Verifica os objectivos do nível. Poderão dar-te uma pista importante sobre o jogo.",
        "No painel dos objectivos tens uma grelha que representa as caixas por onde o robô caminha. Cada quadrado a azul representa um passo. Quantos passos ocorrem até o robô ter de virar, subir, descer" +
        " ou chegar até ao fim?"
    };


}
