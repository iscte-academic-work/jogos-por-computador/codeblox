﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRobot : MonoBehaviour
{
    [SerializeField] AudioClip WalkAudio;
    [SerializeField] AudioClip StartEngineAudio;
    [SerializeField] AudioClip StopEngineAudio;
    [SerializeField] AudioClip RunEngineAudio;
    AudioSource audioPlayer;
    DateTime? endTime=null;
    public bool HasKeyControls=false;
    public GameObject fire;
    public GameObject pointA;
    public GameObject pointB;
    public GameObject pointC;
    public GameObject pointD;
    public Animator rleg;
    public Animator lleg;
    public Animator head;
    public Animator lid;
    public Animator engine;
    Rigidbody rb;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioPlayer = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (HasKeyControls)
        {
            //OPEN and CLOSE LEGs
            if (Input.GetKeyDown("j"))
            {
                if (rleg.GetBool("open") && lleg.GetBool("open"))
                {
                    Foldlegs();
                    Stopwalk();
                }
                else
                {
                    Openlegs();
                }
            }

            //ON and OFF ENGINE
            if (Input.GetKeyDown("k"))
            {
                if (engine.GetBool("ON"))
                {
                    Offengine();
                }
                else
                {
                    Onengine();
                }
            }

            //OPEN and CLOSE HEAD
            if (Input.GetKeyDown("l"))
            {
                if (head.GetBool("open"))
                {
                    Closehead();
                }
                else
                {
                    Openhead();
                }
            }

            //OPEN and CLOSE BACKLID
            if (Input.GetKeyDown("m"))
            {
                if (lid.GetBool("open"))
                {
                    Closelid();
                }
                else
                {
                    Openlid();
                }
            }

            //MOVEMENT
            if (Input.GetKeyDown("i"))
            {
                if (lleg.GetBool("open"))
                {
                    if (lleg.GetBool("walk"))
                    {
                        Stopwalk();
                    }
                    else
                    {
                        Walkstart();
                    }
                }
            }
        }

    }

    public void Openlegs()
    {
        rleg.SetBool("open", true);
        lleg.SetBool("open", true);
    }
    public void Foldlegs()
    {
        rleg.SetBool("open", false);
        lleg.SetBool("open", false);
    }
    
    public void Onengine()
    {
        engine.SetBool("ON", true);
    }

    /// <summary>
    /// Returns false when wait time is not over. Protected from multiple calls
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    public bool TurnOnEngineAndWait(int seconds)
    {
        bool loaded = false;
        endTime = endTime ==null? DateTime.Now.AddSeconds(seconds) : endTime;
        if (!audioPlayer.isPlaying)
        {
            audioPlayer.clip = StartEngineAudio;
            audioPlayer.Play();
        }
        engine.SetBool("ON", true);

        if (DateTime.Compare(DateTime.Now, endTime.Value)>=0)
        {
            loaded = true;
        }
        return loaded;
    }

    public void TurnOffEngine()
    {
        audioPlayer.Stop();
        audioPlayer.loop=false;
        audioPlayer.clip = StopEngineAudio;
        endTime = null;
        engine.SetBool("ON", false);
    }


    public void Offengine()
    {
        engine.SetBool("ON", false);
    }

    public void EngineOn()
    {
        try
        {
            Instantiate(fire, pointA.transform.position, pointA.transform.rotation, pointA.transform);
            Instantiate(fire, pointB.transform.position, pointB.transform.rotation, pointB.transform);
            Instantiate(fire, pointC.transform.position, pointC.transform.rotation, pointC.transform);
            Instantiate(fire, pointD.transform.position, pointD.transform.rotation, pointD.transform);
        }
        catch (Exception) { }
    }

    public void EngineOff()
    {
        try
        {
            Destroy(pointA.transform.Find("FireEffect(Clone)").gameObject);
            Destroy(pointB.transform.Find("FireEffect(Clone)").gameObject);
            Destroy(pointC.transform.Find("FireEffect(Clone)").gameObject);
            Destroy(pointD.transform.Find("FireEffect(Clone)").gameObject);
        }
        catch (Exception) { }
    }

    public bool IsEngineOn()
    {
        return engine.GetBool("ON");
    }

    public void Openhead()
    {
        head.SetBool("open", true);
    }
    public void Closehead()
    {
        head.SetBool("open", false);
    }
    public void RunEngineJetSound()
    {
        audioPlayer.Stop();
        audioPlayer.loop = true;
        audioPlayer.clip = RunEngineAudio;
        audioPlayer.Play();
    }

    public void ResetAudioSource()
    {
        audioPlayer.loop = false;
    }

    public void Walkstart()
    {
        if (!audioPlayer.isPlaying)
        {
            audioPlayer.clip = WalkAudio;
            audioPlayer.Play();
        }
        rleg.SetBool("walk", true);
        lleg.SetBool("walk", true);
    }
    public void Stopwalk()
    {
        //audioPlayer.Stop();
        rleg.SetBool("walk", false);
        lleg.SetBool("walk", false);
    }
    public void Openlid()
    {
        lid.SetBool("open", true);
    }
    public void Closelid()
    {
        lid.SetBool("open", false);
    }
}
