﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

public class CompanionState 
{
    /// <summary>
    /// The name of this state. Must be unique
    /// </summary>
    public string StateName { get; set; }

    /// <summary>
    /// Action to be executed on each state. Returns a string containing the name of the event 
    /// </summary>
    public Func<List<object>, CompanionStateResult> StateAction { get; set; }

    /// <summary>
    /// Indicates if this is the initial state. 
    /// </summary>
    public bool Initial { get; set; }

    /// <summary>
    /// Indicates if this is the final state.
    /// </summary>
    public bool Final { get; set; }


    /// <summary>
    /// Maps an event to the next state
    /// The key of the dictionary is the event name
    /// The value of the dictionary the next state
    /// </summary>
    public Dictionary<CompanionStateEvents, CompanionState> Events { get; set; }

    public CompanionState(string stateName, Func<List<object>, CompanionStateResult> stateAction)
    {
        
        Events = new Dictionary<CompanionStateEvents, CompanionState>();
        StateName = stateName;
        StateAction = stateAction;
    }

}

public class CompanionStateResult
{
    public CompanionStateEvents EventName { get; set; }
    public List<object> NextStateParams { get; set; }

    public CompanionStateResult(CompanionStateEvents eventName, List<object> nextStateParams)
    {
        EventName = eventName;
        NextStateParams = nextStateParams;
    }
}


public enum CompanionStateEvents
{
    START_MACHINE,
    REPEAT,
    SOLVE_BUTTON_PRESSED,
    DONE,
    TIME_ELAPSED,
    NO_HINT,
    HINT_BUTTON_PRESSED,
    HINT1,
    HINT2,
    HINT3,
    CLOSED_DISPLAY,
    DISPLAY_OPENED,
    NO_EVENT,
    DISPLAY_HINT
}