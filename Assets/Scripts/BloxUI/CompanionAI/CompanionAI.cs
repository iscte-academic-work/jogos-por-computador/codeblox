﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// TODO @AT: Os templates para cada um dos bloxes têm de vir de um local diferente do temp container. Isto porque esses bloxes não existem para "sempre"
///             Fazer a eliminação de bloxes no Solve
/// </summary>
public class CompanionAI : MonoBehaviour
{
    public class HintType
    {
        public string MessageTemplate { get; set; }
        public Func<LevelHandler.ObjectiveCheck, bool> ValidationFunction { get; set; }

        public Func<string, string> MessageGenerationFunction { get; set; }
    }


    #region AI Config
    
    [SerializeField] TabManager tabManager;
    [SerializeField] HintTab hintTab;
    [SerializeField] Button SolveButton;
    [SerializeField] bool SolverAvailable;
    [SerializeField] int SolverSecondsToAppear=60;
    private Tuple<List<ObjectiveStep>, ActionSynthesizer> StepsSynthesizer;
    [SerializeField] ForBlox ForBloxTemplate;
    [SerializeField] IfBlox IfBloxTemplate;
    [SerializeField] LogicalOperatorBlox LogicalOperatorBloxTemplate;
    [SerializeField] ControlCharacterBlox WalkBloxTemplate;
    [SerializeField] ControlCharacterBlox TurnRightBloxTemplate;
    [SerializeField] ControlCharacterBlox TurnLeftBloxTemplate;
    [SerializeField] RootBlox RootBlox;
    [SerializeField] LevelHandler LevelHandler;
    /// <summary>
    /// Defines the default minimal pattern size. Used for hint and for solution generation
    /// Note: The pattern list will also have patterns and elements of smaller size. View the comments
    ///         on ActionSynthesizer class
    /// </summary>
    [SerializeField] int MinPatternWindow;
    /// <summary>
    /// Defines the maximum pattern size. Used for hint and for solution generation
    /// </summary>
    [SerializeField] int MaxPatternWindow;
    // Start is called before the first frame update

    /// <summary>
    /// A variation on MinPatternWindow used for solution generation only
    /// </summary>
    [SerializeField] int MinPatternWindowVariation = 0;

    /// <summary>
    /// A variation on MaxPatternWindow used for solution generation. The value of final MaxPatternWindow is clamped if it gets bigger than the allowed
    /// </summary>
    [SerializeField] int MaxPatternWindowVariation = 0;


    DateTime StartTime;
    #endregion AI Config

    #region Timer
    /// <summary>
    /// Time it takes for an hint to be displayer
    /// </summary>
    [SerializeField]
    public int HintTimePeriod;

    /// <summary>
    /// Saves the last time an Hint was displayed
    /// </summary>
    DateTime LastRun;
    #endregion
    bool ScheduleSolution = false;
    bool ScheduleHint = false;
    List<HintType> HintList;
    int LastHintIndex = -1;
    List<CompanionState> CompanionStates;
    CompanionState CurrentState;
    CompanionStateResult LastStateResult;

    // Checks the objectives from a previous run
    LevelHandler.ObjectiveCheck lastCheck;

    void Start()
    {
        LoadStates();
        LoadHints();
        SolveButton.gameObject.SetActive(SolverAvailable);
        CurrentState = CompanionStates.FirstOrDefault(c => c.Initial);
        LastRun = DateTime.Now;
        StartTime = DateTime.Now;
    }

    // Update is called once per frame
    void Update()
    {
        SolveButton.gameObject.SetActive(ShowSolverButton());
        LastStateResult = ProcessEvent(LastStateResult);
    }

    #region State Management

    private bool ShowSolverButton()
    {
        return (DateTime.Now - StartTime).TotalSeconds >= SolverSecondsToAppear && SolverAvailable;
    }

    /// <summary>
    /// Loads the states available for the game engine
    /// </summary>
    public void LoadStates()
    {
        CompanionState startAi = new CompanionState(nameof(StartAI), StartAI);
        startAi.Initial = true;
        CompanionState waiting = new CompanionState(nameof(Waiting), Waiting);
        CompanionState presentSolution = new CompanionState(nameof(PresentSolution), PresentSolution);
        CompanionState randomizeHint = new CompanionState(nameof(RandomizeHint), RandomizeHint); 
        CompanionState displayHelp = new CompanionState(nameof(DisplayHelp), DisplayHelp);

        startAi.Events.Add(CompanionStateEvents.START_MACHINE, waiting);

        waiting.Events.Add(CompanionStateEvents.NO_EVENT, waiting);
        waiting.Events.Add(CompanionStateEvents.HINT_BUTTON_PRESSED, randomizeHint);
        waiting.Events.Add(CompanionStateEvents.TIME_ELAPSED, randomizeHint);
        waiting.Events.Add(CompanionStateEvents.SOLVE_BUTTON_PRESSED, presentSolution);

        presentSolution.Events.Add(CompanionStateEvents.DONE, waiting);

        randomizeHint.Events.Add(CompanionStateEvents.DISPLAY_HINT, displayHelp); 

        displayHelp.Events.Add(CompanionStateEvents.DONE, waiting);

        CompanionStates = new List<CompanionState>() { startAi, waiting, presentSolution, randomizeHint,  displayHelp };
    }

    public void LoadHints()
    {
        HintList = new List<HintType>();
        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.CODE_SUGGESTION,
            ValidationFunction = ShowCheckForSolutionSuggestion,
            MessageGenerationFunction = CodeSuggestion
        });

        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.CHECK_FOR_BLOX_HELP,
            ValidationFunction = ShowViewBloxHelpSuggestion,
            MessageGenerationFunction = NonFormatedHint
        });

        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.UNUSED_BLOXES,
            ValidationFunction = ShowWarningOnUnusedBloxes,
            MessageGenerationFunction = NonFormatedHint
        });

        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.TRY_LEVEL_VARIANT,
            ValidationFunction = ShowTryLevelVariant,
            MessageGenerationFunction = NonFormatedHint
        });
        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.TRY_SOLUTION,
            ValidationFunction = ShowCheckForSolutionSuggestion,
            MessageGenerationFunction = NonFormatedHint
        });
        HintList.Add(new HintType()
        {
            MessageTemplate = CompanionMessages.TRY_SOLUTION,
            ValidationFunction =  (ObjectiveCheck)=> { return true; },
            MessageGenerationFunction = RandomHint
        });
    }



    /// <summary>
    /// This method is responsible for loading states
    /// </summary>
    /// <param name="previousStateResult"></param>
    /// <returns></returns>
    public CompanionStateResult ProcessEvent(CompanionStateResult previousStateResult)
    {
        try
        {
            if (previousStateResult != null)
            {
                CompanionState nextState = CurrentState.Events[previousStateResult.EventName];
                if (nextState != null)
                    CurrentState = nextState;
            }
        }
        catch (KeyNotFoundException)
        {
            //Resets in case of error
            CurrentState = CompanionStates.FirstOrDefault(c => c.Initial);
        }

        return CurrentState.StateAction(previousStateResult == null ? null : previousStateResult.NextStateParams);
    }

    #endregion

    #region States

    public CompanionStateResult StartAI(List<object> stateParams)
    {
        return new CompanionStateResult(CompanionStateEvents.START_MACHINE, null);
    }

    /// <summary>
    /// Waits for time to show hint to elapse, or for a hint button to be clicked   
    /// </summary>
    /// <param name="stateParams"></param>
    /// <returns></returns>
    public CompanionStateResult Waiting(List<object> stateParams)
    {
        CompanionStateEvents stateEvent;

        if (ScheduleSolution)
        {
            stateEvent = CompanionStateEvents.SOLVE_BUTTON_PRESSED;
            ScheduleSolution = false;
        }
        // If Hint button was pressed or time has elapsed
        else if (ScheduleHint || (DateTime.Now.Subtract(LastRun)).TotalSeconds >= HintTimePeriod)
        {
            stateEvent = ScheduleHint ? CompanionStateEvents.HINT_BUTTON_PRESSED : CompanionStateEvents.TIME_ELAPSED;
            ScheduleHint = false;   // To avoid repetition
            LastRun = DateTime.Now; //Resets hint timer
        }
        else //If not hint button pressed or time elapsed
            stateEvent = CompanionStateEvents.NO_EVENT;

        return new CompanionStateResult(stateEvent, null);
    }

    /// <summary>
    /// Solves the problem
    /// </summary>
    /// <param name="stateParams"></param>
    /// <returns></returns>
    public CompanionStateResult PresentSolution(List<object> stateParams)
    {
        if (SolverAvailable)
        {
            RootBlox.RemoveAllChildren();
            int minPatternWindow = MinPatternWindow + UnityEngine.Random.Range(-MinPatternWindowVariation, MinPatternWindowVariation);
            int maxPatternWindow = MaxPatternWindow + UnityEngine.Random.Range(-MaxPatternWindowVariation, MaxPatternWindowVariation);
            Solve(minPatternWindow,maxPatternWindow);
        }
        return new CompanionStateResult(CompanionStateEvents.DONE, null);
    }

    /// <summary>
    /// Creates a "random" hint
    /// </summary>
    /// <param name="stateParams"></param>
    /// <returns></returns>
    public CompanionStateResult RandomizeHint(List<object> stateParams)
    {
        // We are going to choose to display one of three types of hints:
        //      1 - Code suggestion
        //      2 - Generic Help
        CompanionStateEvents stateEvent = CompanionStateEvents.DISPLAY_HINT;

        LevelHandler.ObjectiveCheck objectiveCheck = this.LevelHandler.CheckObjectives(this.RootBlox);
        
        //bool hintAvailable = HintList.Exists(h => h.ValidationFunction(objectiveCheck));

        HintType hint = ListHelper.GetRandomEntry(HintList.Where(h => h.ValidationFunction(objectiveCheck)).ToList());

        string message;
        if (hint != null)
        {
            message = hint.MessageGenerationFunction(hint.MessageTemplate);
        }
        else
        {
            message = CompanionMessages.NO_HINT;
        }

        return new CompanionStateResult(stateEvent, new List<object>() { message });
    }
 
    /// <summary>
    /// Displays a message
    /// </summary>
    /// <param name="stateParams"></param>
    /// <returns></returns>
    public CompanionStateResult DisplayHelp(List<object> stateParams)
    {
        //Expects a unique param
        if (stateParams != null && stateParams.Count > 0)
        {
            string hintMessage = stateParams[0].ToString();
            hintTab.SetHintText(hintMessage);
            tabManager.ForceActive(hintTab.GetComponent<RectTransform>());

        }

        return new CompanionStateResult(CompanionStateEvents.DONE, null);

    }


    #endregion States

    #region Hint generation

    public string NonFormatedHint(string message)
    {
        return message;
    }

    public string RandomHint(string message)
    {
        return ListHelper.GetRandomEntry(CompanionMessages.RANDOM_HINTS);
    }

    public string CodeSuggestion(string message)
    {
        // Gets action synthesizer
        StepsSynthesizer = StepsSynthesizer ?? LevelHandler.GenerateSynthesizer(); // Creates if null
        ActionSynthesizer synthesizer = StepsSynthesizer.Item2;

        ActionSynthesizer.PatternsSolution patternsSolution = synthesizer.ToSolution(MinPatternWindow, MaxPatternWindow);
        // The solution will consist in a tree, being the root element a representation of the root blox
        ActionSynthesizer.SynthesizedSolution solutionRoot = patternsSolution.Solution;

        List<ActionSynthesizer.Pattern> patternList = patternsSolution.PatternList;

        //Now we get a random pattern
        int randIndex = UnityEngine.Random.Range(0, patternList.Count - 1);
        ActionSynthesizer.Pattern pattern = patternList[randIndex];
        Tuple<int, int> patternStepWindow = patternsSolution.GetStepWindow(randIndex);

        //Get this pattern solution
        ActionSynthesizer.SynthesizedSolution patternSolution = solutionRoot.ChildSynthBloxes[randIndex];

        //Gets the total steps
        int numberOfSteps = patternsSolution.ActionStr.Count(c => c == ActionSynthesizer.TileAction.TileActionTypeToChar(ActionSynthesizer.TileAction.TileActionType.WALK));

        //Gets the different bloxes of solution
        Dictionary<ActionSynthesizer.SolutionType, int> bloxesAmount = new Dictionary<ActionSynthesizer.SolutionType, int>();
        patternSolution.GetSolutionBloxTypes(bloxesAmount);
        bloxesAmount.Remove(ActionSynthesizer.SolutionType.ROOT);
        bloxesAmount.Remove(ActionSynthesizer.SolutionType.GROUP);

        string bloxAmountEnum = String.Join(String.Empty, bloxesAmount.Select(b => String.Format(CompanionMessages.CODE_SUGGESTION_BLOX_AMOUNT_TEMPLATE, b.Key, b.Value)));

        return String.Format(message, numberOfSteps, patternStepWindow.Item1, patternStepWindow.Item2, bloxAmountEnum);
    }

    public bool ShowCodeSuggestion(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        return SolverAvailable;
    }

    public bool ShowGenericHelp(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        return ShowWarningOnUnusedBloxes(objectiveCheck) || ShowTryLevelVariant(objectiveCheck) || ShowViewBloxHelpSuggestion(objectiveCheck);

    }

    /// <summary>
    /// Determines if it is valid to warn user about optional or mandatory bloxes
    /// </summary>
    /// <param name="objectiveCheck"></param>
    /// <returns></returns>
    public bool ShowWarningOnUnusedBloxes(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        return !objectiveCheck.mandatoryBloxes || objectiveCheck.numberOfOptionalBloxesUsed < objectiveCheck.maxOptionalBloxesExpected;
    }

    /// <summary>
    /// Determines if it is valid to show a try level variant suggestion.
    /// In opposition to the other objectives, the minutes and attempts won't go back.
    /// </summary>
    /// <param name="objectiveCheck"></param>
    /// <returns></returns>
    public bool ShowTryLevelVariant(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        return objectiveCheck.exceededMinutes > 0 || objectiveCheck.exceededAttempts > 0;
    }

    public bool ShowCheckForSolutionSuggestion(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        
        return ShowSolverButton() || (objectiveCheck.exceededAttempts > 0 && SolverAvailable);
    }



    /// <summary>
    /// Determines if it is valid to suggest user to check help
    /// </summary>
    /// <param name="objectiveCheck"></param>
    /// <returns></returns>
    public bool ShowViewBloxHelpSuggestion(LevelHandler.ObjectiveCheck objectiveCheck)
    {
        // Checks if user has not placed any line since last check or if the user has exceeded the expected lines
        // or if has not placed enough lines
        return lastCheck.usedLines == objectiveCheck.usedLines || objectiveCheck.exceededLines > 0 || (objectiveCheck.usedLines * 1f / objectiveCheck.maxLinesExpected) <= 0.5;
    }

    #endregion


    #region External Events

    /// <summary>
    /// Shall be called from a click event. 
    /// Schedules a hint to be executed on the next oportunity by the state machine
    /// </summary>
    public void ShowHint()
    {
        ScheduleHint = true;
    }

    #endregion


    #region Solution generation
    private void Solve(int minPatternWindow, int maxPatternWindow)
    {

        StepsSynthesizer = StepsSynthesizer ?? LevelHandler.GenerateSynthesizer(); // Creates if null
        ActionSynthesizer synthesizer = StepsSynthesizer.Item2;

        maxPatternWindow = Math.Min( maxPatternWindow,synthesizer.ToString().Length);
        minPatternWindow = Math.Min(Math.Max(0, minPatternWindow), maxPatternWindow);

        ActionSynthesizer.PatternsSolution patternsSolution = synthesizer.ToSolution(minPatternWindow, maxPatternWindow);
        // The solution will consist in a tree, being the root element a representation of the root blox
        ActionSynthesizer.SynthesizedSolution solutionRoot = patternsSolution.Solution;

        GenerateBloxes(RootBlox, solutionRoot.ChildSynthBloxes);

        RootBlox.SetAllBloxesPositionsOnScreen();

    }

    public void ScheduleSolutionRequest()
    {
        ScheduleSolution = true;
    }

    public void GenerateBloxes(ABlox parentBlox, List<ActionSynthesizer.SynthesizedSolution> synthChildren)
    {
        foreach (ActionSynthesizer.SynthesizedSolution child in synthChildren)
        {
            if (child.ChildSynthBloxes.Count > 0) // IF, FOR or GROUP
            {
                // If it is a group does not create a new blox.
                ABlox newParentBlox = child.SynthBloxType == ActionSynthesizer.SolutionType.GROUP ? parentBlox : SolutionTypeToBlox(child.SynthBloxType, parentBlox, child.SynthBloxInputs);
                GenerateBloxes(newParentBlox, child.ChildSynthBloxes);
            }
            else if (child.SynthBloxType == ActionSynthesizer.SolutionType.TURN_LEFT
                || child.SynthBloxType == ActionSynthesizer.SolutionType.TURN_RIGHT
                || child.SynthBloxType == ActionSynthesizer.SolutionType.WALK)
            {
                SolutionTypeToBlox(child.SynthBloxType, parentBlox, null);
            }
        }
    }

    public ABlox SolutionTypeToBlox(ActionSynthesizer.SolutionType solType, ABlox parentBlox, List<String> inputs)
    {
        switch (solType)
        {
            case ActionSynthesizer.SolutionType.FOR:
                ForBlox forBlox = Instantiate(ForBloxTemplate, parentBlox.transform.parent);
                forBlox.gameObject.SetActive(true);
                forBlox.SetInputs(inputs[0], inputs[1], inputs[2]);
                parentBlox.AddToBottomIdented(forBlox, true);
                return forBlox;
                break;
            case ActionSynthesizer.SolutionType.IF:
                LogicalOperatorBlox ifParam = Instantiate(LogicalOperatorBloxTemplate, parentBlox.transform.parent);
                IfBlox ifBlox = Instantiate(IfBloxTemplate, parentBlox.transform.parent);
                ifParam.gameObject.SetActive(true);
                ifBlox.gameObject.SetActive(true);
                ifBlox.AddParam(ifParam);
                parentBlox.AddToBottomIdented(ifBlox, true);
                ifParam.SetValues(inputs[0], false, inputs[1], true, "==");
                return ifBlox;
                break;
            case ActionSynthesizer.SolutionType.TURN_LEFT:
                ControlCharacterBlox tl = Instantiate(TurnLeftBloxTemplate, parentBlox.transform.parent);
                tl.gameObject.SetActive(true);
                parentBlox.AddToBottomIdented(tl, true);
                return tl;
                break;
            case ActionSynthesizer.SolutionType.TURN_RIGHT:
                ControlCharacterBlox tr = Instantiate(TurnRightBloxTemplate, parentBlox.transform.parent);
                tr.gameObject.SetActive(true);
                parentBlox.AddToBottomIdented(tr, true);
                return tr;
                break;
            case ActionSynthesizer.SolutionType.WALK:
                ControlCharacterBlox walkBlx = Instantiate(WalkBloxTemplate, parentBlox.transform.parent);
                walkBlx.gameObject.SetActive(true);
                parentBlox.AddToBottomIdented(walkBlx, true);
                return walkBlx;
                break;
            default:
                throw new Exception("Invalid option");
        }
    }

    #endregion


    private string GetRandomHintType(List<string> forbiddenList, int stringLength = 4)
    {
        forbiddenList = forbiddenList ?? new List<string>();
        int _stringLength = stringLength - 1;
        string randomString = "";
        string[] characters = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        do
        {
            for (int i = 0; i <= _stringLength; i++)
            {
                randomString = randomString + characters[UnityEngine.Random.Range(0, characters.Length)];
            }
        } while (forbiddenList.Exists(str => str == randomString));

        return randomString;
    }



}
