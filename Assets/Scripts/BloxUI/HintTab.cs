﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintTab : MonoBehaviour
{
    [SerializeField] Text HintText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHintText(string hint)
    {
        HintText.text = hint;
    }
}
