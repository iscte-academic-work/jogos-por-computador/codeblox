﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using static LevelDescriptor;
using UnityEngine.UI;

/// <summary>
/// This component will be essencial for the loading of configuration of a level, and evaluation of the level completion
/// Each level will have 5 objectives:
///     1 - Complete in time
///     2 - Use a number of attempts lesser than the maximum
///     3 - Use a number of blocks lesser than the maximum
///     3 - Use the expected bloxes
///     4 - Complete the in game objective
/// </summary>
public class LevelHandler : MonoBehaviour
{
    [SerializeField] Image screenBlocker;
    [SerializeField] LevelDescriptor LevelConfiguration;
    [SerializeField] ObjectivePanel ObjectivePanel;
    [SerializeField] Timer Clock;
    bool screenBlockCanvas = false;


    public struct ObjectiveCheck
    {
        public bool mandatorySteps;   // Did the correct steps
        public bool mandatoryBloxes;  // Used the mandatory bloxes

        public int wrongSteps;
        public double exceededMinutes;         // Extra time used. 
        public double minutesSpent;
        public double maxMinutesExpected;
        public int exceededAttempts;     // Attempts not exceeded
        public int numberOfAttempts;     // Attempts not exceeded
        public int maxAttemptsExpected;     // Attempts not exceeded
        public int exceededLines;        // Used maximum lines
        public int usedLines;        // Used maximum lines
        public int maxLinesExpected;        // Used maximum lines

        public float optionalBloxesUsedPercentage;    // Used the optional bloxes
        public int numberOfOptionalBloxesUsed;    // Used the optional bloxes
        public int maxOptionalBloxesExpected;    // Used the optional bloxes
    }

    public class Evaluation
    {

        public const int MAX_SCORE = 5000;
        public const int SCORE_PER_STAR = 1000;
        public int Score { get; }
        public int Stars { get; set; }
        public bool Success { get; }
        public ObjectiveCheck ObjectiveCheck { get; }

        public int timeDiscount { get; }
        public int optionalBloxDiscount { get; }
        public int linesDiscount { get; }
        public int attemptsDiscount { get; }

        public Evaluation(ObjectiveCheck objectiveCheck)
        {
            this.ObjectiveCheck = objectiveCheck;
            Success = objectiveCheck.mandatorySteps && objectiveCheck.mandatoryBloxes;

            //double discount = 0;

            timeDiscount = Convert.ToInt32(Math.Floor(CalcDiscount(ObjectiveCheck.exceededMinutes) * SCORE_PER_STAR));
            linesDiscount = Convert.ToInt32(Math.Floor(CalcDiscount(ObjectiveCheck.exceededLines) * SCORE_PER_STAR));
            attemptsDiscount = Convert.ToInt32(Math.Floor(CalcDiscount(ObjectiveCheck.exceededAttempts) * SCORE_PER_STAR));
            optionalBloxDiscount = Convert.ToInt32(Math.Floor((1 - ObjectiveCheck.optionalBloxesUsedPercentage) * SCORE_PER_STAR));

            Score = MAX_SCORE - timeDiscount - linesDiscount - attemptsDiscount - optionalBloxDiscount;
            Stars = Convert.ToInt32(Math.Floor(Score * 1.0f / SCORE_PER_STAR));
            //discount -= CalcDiscount(ObjectiveCheck.exceededMinutes);
            //discount -= CalcDiscount(ObjectiveCheck.exceededAttempts);
            //discount -= CalcDiscount(ObjectiveCheck.exceededLines);
            //discount -= 1-ObjectiveCheck.optionalBloxesUsedPercentage;

            //double score = MAX_SCORE + discount;

            //Score = Convert.ToInt32(score * 1000);
            //Stars = Convert.ToInt32(Math.Floor(score));

        }

        // Computes (1-1/(x+1)) in the domain of [0,+inf[
        // If x < 0 returns 0
        public double CalcDiscount(double x)
        {
            return x < 0 || double.IsNaN(x) ? 0 : 1 - 1 / (x + 1);
        }

    }

    int numberOfAttempts = 0;
    private DateTime startTime;
    private List<PlotTile> tilesInScene = new List<PlotTile>();

    // Start is called before the first frame update
    void Start()
    {
        LevelConfiguration.Configure();
        startTime = DateTime.Now;
        BuildTilePlot();
        this.ObjectivePanel.LoadSquareTiles(LevelConfiguration);
        this.ObjectivePanel.LoadTexts(LevelConfiguration);
        SetCharacter();
        numberOfAttempts = 0;

        Clock.LoadTimer(LevelConfiguration.MaxTimeInMinutes);
    }

    // Update is called once per frame
    void Update()
    {
        // Blocks the screen with an invisible image
        screenBlocker.gameObject.SetActive(screenBlockCanvas);

    }

    #region Tile plot handling
    // Builds the tile plot
    public void BuildTilePlot()
    {
        PlotTile templateTile = LevelConfiguration.FirstTile;
        Renderer templateTileRenderer = templateTile.GetComponent<Renderer>();
        // The extents give half of each dimension (width, length, and height)
        Vector3 tileExtents = templateTileRenderer.bounds.extents;
        float tileWidth = 2 * tileExtents.x;
        float tileLength = 2 * tileExtents.z;
        float tileHeight = 2 * tileExtents.y;

        // Important note!
        // The coordinates of a Tile in the Plot Tile do not translate directly to the Coordinates in world
        // plotX <-> worldX, plotY <-> worldZ, plotZ <-> worldY
        // This is to facilitate the configuration of the level, turning it more readable

        //tilesInScene.Add(tile);
        for (int plotY = 0; plotY < LevelConfiguration.PlotLength; plotY++) //iterates through lines (y)
        {
            for (int plotX = 0; plotX < LevelConfiguration.PlotWidth; plotX++) //iterates through columns (x)
            {
                int plotZ = 0;
                //Verifies for this (plotX,plotY) if there is a special coordinate that modifies heigth
                Vector3Int specialCoordinate = LevelConfiguration.SpecialCoordinates.Where(a => a.x == plotX && a.y == plotY).FirstOrDefault();
                if (specialCoordinate != null)
                {
                    plotZ = specialCoordinate.z;
                }

                // Gets a new tile (or reuses the template if we are on the first position)
                PlotTile newTile = plotY == 0 && plotX == 0 ? templateTile : Instantiate(templateTile);
                newTile.PlotPosition = new Vector3Int(plotX, plotY, plotZ);

                //Defines the new tile position in the world
                Vector3 newTilePosition = newTile.transform.position;
                newTilePosition.x += plotX * tileWidth;
                newTilePosition.y += plotZ * tileHeight;
                newTilePosition.z += plotY * tileLength;

                //Sets the new position
                newTile.transform.position = newTilePosition;

                //Saves the tile in the tile list
                tilesInScene.Add(newTile);
            }
        }
    }

    public void ResetTilePlotState()
    {
        if (tilesInScene != null)
            tilesInScene.ForEach(tile => { tile.Reset(); });
    }
    #endregion


    #region Hints and evaluation

    public ObjectiveCheck CheckObjectives(RootBlox rootBlox)
    {
        ObjectiveCheck check;
        int stars = 0;
        /// Each level will have 5 objectives:
        ///     1 - Complete in time
        ///     2 - Use a number of attempts lesser than the maximum
        ///     3 - Use a number of blocks lesser than the maximum
        ///     4 - Use the expected optional bloxes 
        ///     5 - Complete the in game objective (mandatory steps, bloxes, special actions)
        ///     

        // 1. Time evaluation
        DateTime expectedEndTime = startTime.AddMinutes(LevelConfiguration.MaxTimeInMinutes);
        check.exceededMinutes = (DateTime.Now - expectedEndTime).TotalMinutes;
        check.minutesSpent = (DateTime.Now - startTime).TotalMinutes;
        // 2. Number of attempts
        check.exceededAttempts = numberOfAttempts - (int)LevelConfiguration.MaxAttempts;
        check.numberOfAttempts = numberOfAttempts;
        // 3. Use a number of code lines lesser than the maximum
        check.exceededLines = rootBlox.GetChildBloxListInVerticalOrder().Count - (int)LevelConfiguration.MaxCodeLinesExpected;
        check.usedLines = rootBlox.GetChildBloxListInVerticalOrder().Count;

        // 4. Right steps. This is a mandatory object, so level shall fail when not accomplished
        // 4.1 Get all the mandatory steps that were executed, with the special action included
        // The final count should match the count of LevelConfiguration.MandatorySteps
        //var queryMandatoryStepsCompleted = from mandatoryStep in LevelConfiguration.MandatorySteps
        //                                   join plotTile in tilesInScene on mandatoryStep.CoordinateInPlot equals plotTile.PlotPosition
        //                                   where mandatoryStep != null && plotTile != null && plotTile.SpecialActionExecuted == mandatoryStep.SpecialAction && plotTile.Stepped
        //                                   select plotTile;

        var queryMandatoryStepsCompleted = LevelConfiguration.MandatorySteps.Where(ms => tilesInScene.Exists(t => t.Stepped && t.SpecialActionExecuted == ms.SpecialAction && t.PlotPosition == ms.CoordinateInPlot)).ToList();

        bool allTheMandatoryStepsDone = queryMandatoryStepsCompleted.Count() == LevelConfiguration.MandatorySteps.Count;

        // 4.2 Get all the the not expected steps. It is mandatory that the user restricts to the mandatory steps
        var queryNotExpectedSteps = from plotTile in tilesInScene
                                    join mandatoryStep in LevelConfiguration.MandatorySteps on plotTile.PlotPosition equals mandatoryStep.CoordinateInPlot into mSteps
                                    from step in mSteps.DefaultIfEmpty()
                                    where step == null && plotTile.Stepped
                                    select plotTile;

        bool notExpectedStepsExist = queryNotExpectedSteps.Count() > 0;

        check.wrongSteps = queryNotExpectedSteps.Count();
        check.mandatorySteps = !notExpectedStepsExist && allTheMandatoryStepsDone;


        // 5. This list contains the distinct blox types, and their count
        List<Tuple<Type, int>> usedBloxTypeCount = rootBlox.GetAllBloxesBellow().GroupBy(b => b.GetType()).Select(g => new Tuple<Type, int>(g.First().GetType(), g.Count())).ToList();

        List<ExpectedBlox> mandatoryBloxesFullyUsed = LevelConfiguration.MandatoryBloxes.Where(mb => usedBloxTypeCount.Exists(b => b.Item1 == mb.BloxType && b.Item2 >= mb.MinimumQuantity)).ToList();

        check.mandatoryBloxes = mandatoryBloxesFullyUsed.Count == LevelConfiguration.MandatoryBloxes.Count();


        List<ExpectedBlox> expectedOptionalBloxesUsed = LevelConfiguration.OptionalExpectedBloxes.Where(mb => usedBloxTypeCount.Exists(b => b.Item1 == mb.BloxType)).ToList();
        int numberOfOptionalBloxesUsed = usedBloxTypeCount
                                            // Gets all the used bloxes that are of the type of one expected to be used
                                            .Where(b => LevelConfiguration.OptionalExpectedBloxes.Exists(mb => mb.BloxType == b.Item1))
                                            // Gets the amount used. If the amount used is bigger than the minimum expected, uses the later value
                                            // If it is expected to use 2 bloxes of Int, and 1 of If, we don't want to classify a full use of option bloxes
                                            // if the user put 3 Int bloxes
                                            .Select(b => Math.Min(b.Item2, LevelConfiguration.OptionalExpectedBloxes.First(o => o.BloxType == b.Item1).MinimumQuantity)).Sum();


        check.optionalBloxesUsedPercentage = LevelConfiguration.OptionalExpectedBloxes.Count == 0 ? 1 : numberOfOptionalBloxesUsed * 1f / LevelConfiguration.OptionalExpectedBloxes.Select(o => o.MinimumQuantity).Sum();

        check.maxAttemptsExpected = Convert.ToInt32(LevelConfiguration.MaxAttempts);
        check.maxLinesExpected = Convert.ToInt32(LevelConfiguration.MaxCodeLinesExpected);
        check.maxMinutesExpected = Convert.ToInt32(LevelConfiguration.MaxTimeInMinutes);
        check.maxOptionalBloxesExpected = LevelConfiguration.OptionalExpectedBloxes.Select(o => o.MinimumQuantity).Sum();
        check.numberOfOptionalBloxesUsed = numberOfOptionalBloxesUsed;

        return check;
    }

    public Evaluation EvaluateLevel(RootBlox rootBlox)
    {

        return new Evaluation(CheckObjectives(rootBlox));
    }

    public void incrementAttempts()
    {
        numberOfAttempts++;
    }


    /// <summary>
    /// Gets ordered objective steps. They are ordered according to the step the character does
    /// By default it will search the tile in front of the character, and if there isn't, it search left and right
    /// 
    /// Returns an ordered list of mandatory steps, and synthesized solution of what the character has to do
    /// </summary>
    public Tuple<List<ObjectiveStep>, ActionSynthesizer> GenerateSynthesizer()
    {
        List<CharacterOrientation> clockwiseSeq = new List<CharacterOrientation>() { CharacterOrientation.NORTH, CharacterOrientation.EAST,
                                                                                    CharacterOrientation.SOUTH, CharacterOrientation.WEST };

        List<CharacterOrientation> counterClockwiseSeq = new List<CharacterOrientation>() { CharacterOrientation.NORTH, CharacterOrientation.WEST,
                                                                                    CharacterOrientation.SOUTH, CharacterOrientation.EAST };

        Vector3Int firstStep = LevelConfiguration.CharacterStartPointInPlot;
        //List<ObjectiveStep> originalStepList = LevelConfiguration.MandatorySteps.Where(s => s.CoordinateInPlot == firstStep).ToList();
        List<ObjectiveStep> originalStepList = LevelConfiguration.MandatorySteps;
        List<ObjectiveStep> orderedList = new List<ObjectiveStep>();
        ActionSynthesizer actionSynthesizer = new ActionSynthesizer();

        bool stop = false;

        Vector3Int currentStep = firstStep;
        orderedList.Add(originalStepList.First(s => s.CoordinateInPlot == firstStep));
        CharacterOrientation currentOrientation = LevelConfiguration.CharacterInitialOrientation;
        int numberOfTiles = tilesInScene.Count;
        int killCount = 0; //This is to prevent infinite loop
        while (!stop && killCount < numberOfTiles)
        {
            //SynthesizedSolution.TileAction synthAction = new SynthesizedSolution.TileAction();
            //synthAction.Action = SynthesizedSolution.TileAction.TileActionType.WALK;
            bool? leftRight = null; //left=true; right = false

            bool foundStep = false;
            // At the current position, we search for any valid step around      
            // So we iterate through the 3 possible solutions (character can either go front, left, right)

            for (int i = 0; i < 3 && !foundStep; i++)
            {
                // When i = 0, we search in the current direction of the character
                CharacterOrientation tempOrientation = currentOrientation;
                // When i = 1, we search for a valid step at the right
                if (i == 1)
                {
                    leftRight = false;
                    int indexInList = clockwiseSeq.IndexOf(tempOrientation);
                    int nextIndex = indexInList + 1;
                    nextIndex = nextIndex + 1 > clockwiseSeq.Count ? 0 : nextIndex;
                    tempOrientation = clockwiseSeq[nextIndex];
                }
                // When i = 2, we search for a valid step at the left
                else if (i == 2)
                {
                    leftRight = true;
                    int indexInList = counterClockwiseSeq.IndexOf(tempOrientation);
                    int nextIndex = indexInList + 1;
                    nextIndex = nextIndex + 1 > counterClockwiseSeq.Count ? 0 : nextIndex;
                    tempOrientation = counterClockwiseSeq[nextIndex];
                }

                // Next we determine a potential step
                int sumX = 0, sumY = 0;
                switch (tempOrientation)
                {
                    case CharacterOrientation.NORTH:
                        sumY = 1;
                        break;
                    case CharacterOrientation.SOUTH:
                        sumY = -1;
                        break;
                    case CharacterOrientation.EAST:
                        sumX = 1;
                        break;
                    case CharacterOrientation.WEST:
                        sumX = -1;
                        break;
                }
                Vector3Int potentialNextStep = currentStep;
                potentialNextStep.x += sumX;
                potentialNextStep.y += sumY;

                // And verify if it exists on the mandatory step list
                ObjectiveStep potObjStep = originalStepList.Where(s => s.CoordinateInPlot.x == potentialNextStep.x && s.CoordinateInPlot.y == potentialNextStep.y).FirstOrDefault();

                // If a valid step is found, stops this cicle, and adds the step to the potential list
                if (potObjStep != null)
                {
                    foundStep = true;
                    orderedList.Add(potObjStep);

                    ActionSynthesizer.TileAction walkAction = new ActionSynthesizer.TileAction();
                    walkAction.Action = ActionSynthesizer.TileAction.TileActionType.WALK;
                    ActionSynthesizer.TileAction turnAction = null;

                    if (leftRight.HasValue)
                    {
                        turnAction = new ActionSynthesizer.TileAction()
                        {
                            Action = leftRight.Value ? ActionSynthesizer.TileAction.TileActionType.TURN_LEFT : ActionSynthesizer.TileAction.TileActionType.TURN_RIGHT
                        };
                        currentOrientation = tempOrientation;
                    }

                    actionSynthesizer.Actions.Add(turnAction);
                    actionSynthesizer.Actions.Add(walkAction);


                    currentStep = potObjStep.CoordinateInPlot;
                }
                // If not, attempts another orientation
                else
                {
                    int indexInList = clockwiseSeq.IndexOf(tempOrientation);
                    int nextIndex = indexInList + 1;
                    nextIndex = nextIndex + 1 > clockwiseSeq.Count ? 0 : nextIndex;
                    tempOrientation = clockwiseSeq[nextIndex];
                }
            }

            // The execution stops when not step found, or when a loop is closed
            // The step where the loop is closed will be still registered

            //Verifies if loop is closed
            if (currentStep == firstStep)//if(orderedList.FirstOrDefault(s=>s.CoordinateInPlot == currentStep) != null)
            {
                stop = true;
            }
            // If loop was closed or no next step found
            stop = stop || !foundStep;

            // we must make sure that the character stops when a loop closes
            // we need to check for oposite coordinates, for the character to not go back 
            killCount++;
        }


        return new Tuple<List<ObjectiveStep>, ActionSynthesizer>(orderedList, actionSynthesizer);
    }


    #endregion

    #region Character handling
    public void SetCharacter()
    {
        CubotController cubot = LevelConfiguration.Character;
        cubot.SetUp(LevelConfiguration, tilesInScene);
    }


    #endregion


    public void BlockScreen(bool lockScreen)
    {
        screenBlockCanvas = lockScreen;
    }

    //Returns the proportion between minutes elapsed to maximum time
    public double TimeElapsedProportion()
    {
        int maxMinutes = Convert.ToInt32(LevelConfiguration.MaxTimeInMinutes);
        double minutesElapsed = (DateTime.Now - startTime).TotalMinutes;
        return minutesElapsed / maxMinutes;
    }
}

