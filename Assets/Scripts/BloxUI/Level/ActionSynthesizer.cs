﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;


// This class describes a proposed sequence of walks and turns the character has to perform
// This assumes character does first the turns and then walks
// 
public class ActionSynthesizer
{
    public enum SolutionType
    {
        ROOT,
        GROUP, // GROUP will just group elements that won't belong to a group. 
        FOR,
        IF,
        TURN_LEFT,
        TURN_RIGHT,
        WALK
    }

    /// <summary>
    /// Describes a structure for a possible solution for a level
    /// </summary>
    public class SynthesizedSolution
    {
        public SolutionType SynthBloxType { get; set; }
        /// <summary>
        /// Only FOR and IF have inputs.
        /// In case of for, inputs will be iteratorname, fromVal and toVal
        /// IFs will occur in the context of being inside a FOR, so their inputs will be just one index of a FOR loop that they will evaluate
        /// 
        /// </summary>
        public List<String> SynthBloxInputs { get; set; }
        public List<SynthesizedSolution> ChildSynthBloxes { get; set; }
        public SynthesizedSolution()
        {
            SynthBloxInputs = new List<string>();
            ChildSynthBloxes = new List<SynthesizedSolution>();
        }

        public static SynthesizedSolution CreateFor(string iteratorName, string fromVal, string toVal)
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.FOR,
                SynthBloxInputs = new List<string>() { iteratorName, fromVal, toVal }
            };
        }

        public static SynthesizedSolution CreateWalk()
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.WALK
            };
        }

        public static SynthesizedSolution CreateIf(string varName, string varValue)
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.IF,
                SynthBloxInputs = new List<string>() { varName, varValue }
            };
        }

        public static SynthesizedSolution CreateTurnLeft()
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.TURN_LEFT
            };
        }

        public static SynthesizedSolution CreateTurnRight()
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.TURN_RIGHT
            };
        }

        public static SynthesizedSolution CreateRoot()
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.ROOT
            };
        }

        public static SynthesizedSolution CreateGroup()
        {
            return new SynthesizedSolution()
            {
                SynthBloxType = SolutionType.GROUP
            };
        }

        public void GetSolutionBloxTypes(Dictionary<SolutionType, int> bloxAmount)
        {

            bloxAmount[this.SynthBloxType] = !bloxAmount.ContainsKey(this.SynthBloxType) ? 1 : bloxAmount[this.SynthBloxType] + 1;
            foreach (SynthesizedSolution sol in this.ChildSynthBloxes)
            {
                sol.GetSolutionBloxTypes(bloxAmount);
            }
        }

    }


    public class TileAction
    {
        public enum TileActionType
        {
            // Character just walks
            WALK,
            // Character turns left
            TURN_LEFT,
            // Character turns right
            TURN_RIGHT
        }
        public TileActionType Action { get; set; }

        public override string ToString()
        {
            return Action == TileActionType.WALK ? "w" : (Action == TileActionType.TURN_LEFT ? "l" : "r");
        }

        public static TileActionType StringToTileAction(string str)
        {
            switch (str)
            {
                case "w":
                    return TileActionType.WALK;
                case "l":
                    return TileActionType.TURN_LEFT;
                case "r":
                    return TileActionType.TURN_RIGHT;
                default:
                    throw new Exception("Not valid action string");

            }
        }

        public static char TileActionTypeToChar(TileActionType actionType)
        {
            switch (actionType)
            {
                case TileActionType.WALK:
                    return 'w';
                case TileActionType.TURN_LEFT:
                    return 'l';
                case TileActionType.TURN_RIGHT:
                    return 'r';
                default:
                    return '\0';

            }
        }
    }

    /// <summary>
    /// Describes a pattern in tile plot and the index at which it repeats
    /// </summary>
    public class Pattern
    {
        public string Actions { get; set; }
        public IEnumerable<int> Indexes { get; set; }
    }

    public class PatternsSolution
    {
        public string ActionStr { get; }
        public List<Pattern> PatternList { get; set; }

        public SynthesizedSolution Solution { get; set; }

        public PatternsSolution(string actionStr, List<Pattern> patterns, SynthesizedSolution solution)
        {
            ActionStr = actionStr;
            PatternList = patterns;
            Solution = solution;
        }

        /// <summary>
        /// Given an index of pattern list, gets the first and final steps index
        /// Ex: actionStr = wwwlwwrwwrwwl, and we are evaluating the pattern 2xwwr
        ///     We have in this example a total of 9 walks (0 to 8), and the pattern 2xwwr
        ///     goes from 3 to 6 (total of 4 steps). 3 and 6 are the are the first and final steps of this pattern
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Tuple<int, int> GetStepWindow(int index)
        {
            Pattern pattern = PatternList[index];
            int minIndex = pattern.Indexes.Min();
            int maxIndex = minIndex+(pattern.Actions.Length*pattern.Indexes.Count()-1);// pattern.Indexes.Max();

            // Now we analyse the amount of turn operations before (and including) min and max indexes
            // and subtract to those, those amounts
            string actionsTilMin = ActionStr.Substring(0, minIndex + 1);
            string actionsTilMax = ActionStr.Substring(0, maxIndex + 1);

            // Turn left
            char turnLeftChar = TileAction.TileActionTypeToChar(TileAction.TileActionType.TURN_LEFT);
            char turnRightChar = TileAction.TileActionTypeToChar(TileAction.TileActionType.TURN_RIGHT);

            int amountTurnsAtMin = actionsTilMin.Count(c => c == turnLeftChar || c == turnRightChar);
            int amountTurnsAtMax = actionsTilMax.Count(c => c == turnLeftChar || c == turnRightChar);

            return new Tuple<int, int>(minIndex - amountTurnsAtMin, maxIndex - amountTurnsAtMax);
        }

    }


    public List<TileAction> Actions { get; set; }

    public ActionSynthesizer()
    {
        Actions = new List<TileAction>();
    }

    public override string ToString()
    {
        string str = string.Join("", Actions);
        return str == null ? String.Empty : str;
    }

    //Finds all occurrences of a string, and organizes them by contiguity if organizeContiguous is true. 
    private List<List<int>> AllIndexesOf(string str, string value, bool organizeContiguous = false)
    {
        List<List<int>> occurences = new List<List<int>>();

        List<int> contiguousIndexes = new List<int>();
        occurences.Add(contiguousIndexes);

        if (String.IsNullOrEmpty(value))
            throw new ArgumentException("the string to find may not be empty", "value");

        int previousIdx = 0;
        bool first = true;
        for (int index = 0; ; index += value.Length)
        {
            index = str.IndexOf(value, index);

            if (index == -1)
                break;

            if (organizeContiguous)
            {
                // If not contiguous, organizes the index in a new list
                if (!(index == previousIdx + value.Length) && index > 0 && !first)
                {
                    contiguousIndexes = new List<int>();
                    occurences.Add(contiguousIndexes);
                }
            }

            contiguousIndexes.Add(index);
            previousIdx = index;
            first = false;

        }
        return occurences;
    }

    // Gets all the index intervals of the pattern
    private List<Tuple<int, int>> PatternIndexIntervals(List<Pattern> patterns)
    {

        List<Tuple<int, int>> intervals = null;
        try
        {
            intervals = new List<Tuple<int, int>>();
            foreach (Pattern pattern in patterns)
            {
                int actionsLength = pattern.Actions.Length;
                int firstIndex = pattern.Indexes.Min();
                int lastIndex = pattern.Indexes.Max() + actionsLength - 1;
                intervals.Add(new Tuple<int, int>(firstIndex, lastIndex));
            }
        }
        catch (Exception ex)
        {
            intervals = new List<Tuple<int, int>>();
        }

        return intervals;
    }


    private bool IntervalsIntersect(Tuple<int, int> interval1, Tuple<int, int> interval2)
    {
        return IsWithin(interval1.Item1, interval2.Item1, interval2.Item2) || IsWithin(interval1.Item2, interval2.Item1, interval2.Item2)
            || IsWithin(interval2.Item1, interval1.Item1, interval1.Item2) || IsWithin(interval2.Item2, interval1.Item1, interval1.Item2);
    }

    public bool IsWithin(int val, int v1, int v2)
    {
        return val >= v1 && val <= v2;
    }

    // Gets the next valid index for a given window size an pattern list
    private int nextValidIndexInPatterns(List<Pattern> patterns, int proposedIndex, string originalString, int windowSize, int MaxRecursionDepth = 50)
    {
        if (MaxRecursionDepth <= 0)
            return -1;

        int nextIndex = proposedIndex;
        if (patterns.Count <= 0) return nextIndex;
        List<Tuple<int, int>> intervals = PatternIndexIntervals(patterns);
        // Finds if index is contained in one interval
        Tuple<int, int> interval = intervals.Where(i => i.Item1 <= proposedIndex && i.Item2 >= proposedIndex).FirstOrDefault();

        if (interval != null)
        {
            nextIndex = interval.Item2 + 1;
        }

        //validates according to window size. Verifies if the next possible index plus window size (last index) fits inside another interval, or if intervals intersect
        // If so attempts to get another index
        int nextLastIndex = nextIndex + windowSize - 1;
        Tuple<int, int> nextInterval = new Tuple<int, int>(nextIndex, nextLastIndex);
        //Tuple<int, int> intersectedInterval = intervals.FirstOrDefault(i => (i.Item1 <= nextLastIndex && i.Item2 >= nextLastIndex)
        //                        || (nextIndex <= i.Item1 && nextLastIndex >= i.Item1)
        //                        || (nextIndex <= i.Item2 && nextLastIndex >= i.Item2));
        if (intervals.Exists(i => IntervalsIntersect(nextInterval, i)))
        {
            nextIndex = intervals.Where(i => IntervalsIntersect(nextInterval, i)).Max(a => a.Item2) + 1;
            nextIndex = nextValidIndexInPatterns(patterns, nextIndex, originalString, windowSize, MaxRecursionDepth-1);
        }

        //If out of range
        if (nextIndex >= originalString.Length - 1)
            nextIndex = -1;

        return nextIndex;
    }

    //Validates if a given string is already covered by the pattern
    private bool isPatternFull(List<Pattern> patterns, string originalString)
    {
        int sum = 0;
        List<Tuple<int, int>> intervals = PatternIndexIntervals(patterns);
        foreach (Tuple<int, int> interval in intervals)
        {
            sum += interval.Item2 - interval.Item1 + 1;
        }

        return sum == originalString.Length;
    }


    private void GetPatterns(List<Pattern> patterns, string actionsStr, int idealMinWindowSize, int windowSize)
    {
        GetPatterns(patterns, actionsStr, idealMinWindowSize, windowSize, 2);
        GetPatterns(patterns, actionsStr, 0, windowSize, 1);

    }


    /// <summary>
    /// Converts the pre defined tile action list to a pattern list.
    /// For example: a tile action list converts to wwwlwwwlwwrwwrwwwl
    /// and idealMinWindowSize = 3 and maxWindowSize = 4
    /// 
    /// The pattern reading will have two phases. 
    /// 1st: detect patterns that repeat more than once
    /// 
    /// A window will "run" in the string. 
    /// In a first iteration reads a segment inside window: |wwwl|wwwlwwrwwrwwwl, being the segment "wwwl".
    /// Then it finds all contiguous occurrences of "wwwl", getting two sequences: 2xwwwl and 1xwwwl. Only saves the first
    /// because for now we are only watching sequences that repeat more than once. 
    /// After this finds the next valid index in string (a valid index means an index in string which resides on the window [index, index+windowSize-1])
    /// and repeats this previous logic. When it gets to the end, decrements the window size by 1, and repeats everything for this smaller window
    /// until a the idealMinWindowSize.
    /// 
    /// 2nd: detect patterns that may not repeat (in fact they are not patterns)
    /// Similar to the previous. Only goes throught not read parts of the tile action string, and accepts patterns that don't repeat
    /// 
    /// 
    /// </summary>
    /// <param name="idealMinWindowSize"></param>
    /// <param name="maxWindowSize"></param>
    /// <returns></returns>
    public List<Pattern> ToPatterns(int idealMinWindowSize, int maxWindowSize)
    {
        string actionsStr = this.ToString();
        List<Pattern> patterns = new List<Pattern>();
        GetPatterns(patterns, actionsStr, idealMinWindowSize, maxWindowSize);
        return patterns;
    }

    /// <summary>
    /// Analyses the patterns generated and gets a list of patterns and a possible solution
    /// Each child in solution corresponds to a solution for a pattern
    /// </summary>
    /// <param name="idealMinWindowSize"></param>
    /// <param name="maxWindowSize"></param>
    /// <returns></returns>
    public PatternsSolution ToSolution(int idealMinWindowSize, int maxWindowSize)
    {
        string actionsStr = this.ToString();
        List<string> usedNames = new List<string>(); //saves a list of names already used in iterators, to avoid repetition
        List<Pattern> patterns = new List<Pattern>();

        GetPatterns(patterns, actionsStr, idealMinWindowSize, maxWindowSize);

        char walkChar = TileAction.TileActionTypeToChar(TileAction.TileActionType.WALK);
        char turnRightChar = TileAction.TileActionTypeToChar(TileAction.TileActionType.TURN_RIGHT);
        char turnLeftChar = TileAction.TileActionTypeToChar(TileAction.TileActionType.TURN_LEFT);

        SynthesizedSolution root = SynthesizedSolution.CreateRoot();
        // For each pattern we analyse the amount of repetitions and foreach
        // repetition the amount of walks and turns. 
        foreach (Pattern pattern in patterns)
        {
            SynthesizedSolution patternSolution;

            int patternRepetitions = pattern.Indexes.Count();
            string patternActionsStr = pattern.Actions;
            // wwwl             wwwwlwb
            // We go through the string. The amount of walks count for the iteration count
            int walkAmount = patternActionsStr.ToArray().Count(c => c == walkChar);

            // Then we find the indexes of turns (rights and lefts).
            List<int> trOccurences = AllIndexesOf(patternActionsStr, turnRightChar.ToString())[0];
            List<int> tlOccurences = AllIndexesOf(patternActionsStr, turnLeftChar.ToString())[0];

            int turnAmount = trOccurences.Count + tlOccurences.Count;

            // First build solution for the pattern itself

            // We dont build a walk loop in the following conditions: one or none walks. Ex: "w", "wl", "rw", "lwr"
            // In this scenario, instead of adding walks and turns inside loops and ifs, we just add them in the order they appear in
            if (walkAmount <= 1)
            {
                patternSolution = SynthesizedSolution.CreateGroup();
                foreach (char action in patternActionsStr)
                {
                    TileAction.TileActionType actionType = TileAction.StringToTileAction(action.ToString());
                    SynthesizedSolution sol;
                    if (actionType == TileAction.TileActionType.WALK)
                    {
                        sol = SynthesizedSolution.CreateWalk();
                    }
                    else
                    {
                        sol = actionType == TileAction.TileActionType.TURN_LEFT ? SynthesizedSolution.CreateTurnLeft() : SynthesizedSolution.CreateTurnRight();
                    }
                    patternSolution.ChildSynthBloxes.Add(sol);

                }
            }
            // Otherwise, we have to synthesize the actions inside a loop Ex: "www", "ww", "wwr", "lwwwr", "lwww", "wwwlar"
            else
            {
                List<Tuple<int, char>> tOccurences = trOccurences.Select(idx => new Tuple<int, char>(idx, turnRightChar)).ToList();
                tOccurences.AddRange(tlOccurences.Select(idx => new Tuple<int, char>(idx, turnLeftChar)).ToList());
                tOccurences = tOccurences.OrderBy(t => t.Item1).ToList();

                // Creates the loop that iterates over the walk's
                string iteratorName = CreateRandomString(usedNames);
                usedNames.Add(iteratorName);
                SynthesizedSolution walkLoop = SynthesizedSolution.CreateFor(iteratorName, 0.ToString(), (walkAmount - 1).ToString());

                //If the pattern begins with a turn, we must perform it before start "walking"
                if (patternActionsStr[0] == turnLeftChar || patternActionsStr[0] == turnRightChar)
                {
                    //Creates the if
                    SynthesizedSolution ifCondtion = SynthesizedSolution.CreateIf(iteratorName, (0).ToString());
                    ifCondtion.ChildSynthBloxes.Add(patternActionsStr[0] == turnLeftChar ? SynthesizedSolution.CreateTurnLeft() : SynthesizedSolution.CreateTurnRight());
                    //Adds it to the walkLoop
                    walkLoop.ChildSynthBloxes.Add(ifCondtion);
                }

                if (walkAmount > 0)
                    walkLoop.ChildSynthBloxes.Add(SynthesizedSolution.CreateWalk()); //adds a walk as child


                // Creates the if statements that will control the turns

                // For each occurence of a turn left or right, adds an if for that index-1-i
                // i counts the amount of turns already added
                // Imagine the following: wwwlwr
                // This produces a for loop of 4 iterations (there are four walks), from 0 to 3. 
                // The first turn (left) is at index 3 in string, and the second (right) is at index 5
                // We want the "l" to execute at the same index of the 3rd "w" (at index 2 in string, and index 2 in the for loop)
                // We want the "r" to execute at the same index of the 4th "w" (at index 4 in string, and index 3 in the for loop)
                // So we subtract to the index of the turn in string a value of 1, and the amount of turns already added, for it to execute
                // at the same for loop iteration of the last walk in string
                int i = 0;
                foreach (Tuple<int, char> turn in tOccurences)
                {
                    if (turn.Item1 > 0)
                    {
                        //Creates the if
                        SynthesizedSolution ifCondtion = SynthesizedSolution.CreateIf(iteratorName, (turn.Item1 - 1 - i).ToString());
                        ifCondtion.ChildSynthBloxes.Add(turn.Item2 == turnLeftChar ? SynthesizedSolution.CreateTurnLeft() : SynthesizedSolution.CreateTurnRight());
                        //Adds it to the walkLoop
                        walkLoop.ChildSynthBloxes.Add(ifCondtion);
                        i++;
                    }
                }



                // Checks if this patterns repeats more than once, and if so,
                // creates a loop that repeats the walk loop as many times needed
                if (patternRepetitions > 1)
                {
                    iteratorName = CreateRandomString(usedNames);
                    usedNames.Add(iteratorName);
                    patternSolution = SynthesizedSolution.CreateFor(iteratorName, 0.ToString(), (patternRepetitions - 1).ToString());
                    patternSolution.ChildSynthBloxes.Add(walkLoop);
                }
                else
                {
                    patternSolution = walkLoop;
                }

            }

            // Then, if pattern repeats, encapsulates it in a loop
            if(patternRepetitions > 1)
            {
                string iteratorName = CreateRandomString(usedNames);
                usedNames.Add(iteratorName);
                SynthesizedSolution repetitionLoop = SynthesizedSolution.CreateFor(iteratorName, 0.ToString(), (patternRepetitions - 1).ToString());

                repetitionLoop.ChildSynthBloxes.Add(patternSolution);
                root.ChildSynthBloxes.Add(repetitionLoop);
            }
            else
            {
                root.ChildSynthBloxes.Add(patternSolution);
            }
        }
        PatternsSolution patternsSolution = new PatternsSolution(actionsStr, patterns, root);

        return patternsSolution;
    }

    private string CreateRandomString(List<string> forbiddenList, int stringLength = 4)
    {
        forbiddenList = forbiddenList ?? new List<string>();
        int _stringLength = stringLength - 1;
        string randomString = "";
        string[] characters = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        do
        {
            for (int i = 0; i <= _stringLength; i++)
            {
                randomString = randomString + characters[UnityEngine.Random.Range(0, characters.Length)];
            }
        } while (forbiddenList.Exists(str => str == randomString));

        return randomString;
    }


    public List<Pattern> testPatterns()
    {
        List<Pattern> patterns = new List<Pattern>();
        GetPatterns(patterns, "aaaabaaabaaabaaabaaabcdaaab", 3, 4);

        List<Pattern> patterns2 = new List<Pattern>();
        GetPatterns(patterns2, "aaabaaabaaabaaabaaabcdaaab", 3, 4);

        List<Pattern> patterns3 = new List<Pattern>();
        GetPatterns(patterns3, "aaaabaaabaaabaaabaaabcdaaab", 4, 8);

        List<Pattern> patterns4 = new List<Pattern>();
        GetPatterns(patterns4, "aaaabaaabaacbaacbaaabcdaaab", 3, 5);

        return patterns;
    }


    /// <summary>
    /// Gets repetition patterns in string, based on a windowSize.
    /// Ex: aaabaaabaaabaaabaaabcdaaab
    /// For a window size of 4 it returns 5xaaab 1xcd 1x aaab
    /// For a window size of 8 it returns 2xaaabaaab 1xaaab 1xcd 1xaaab
    /// </summary>
    /// <param name="actionsStr"></param>
    /// <param name="windowSize"></param>
    /// <returns></returns>
    public void GetPatterns(List<Pattern> patterns, string actionsStr, int idealMinWindowSize, int windowSize, int minimalOccurences)
    {
        if (isPatternFull(patterns, actionsStr) || windowSize < idealMinWindowSize)
            return;

        int i = 0;

        // 1. Check for patterns that repeat more than once for this window size
        // ex: aaaabacaaabacaabaaabaca  and windowSize = 5 =>  1st |aaaab|acaaabacaabaaabaca ; 2nd a|aaaba|caaabacaabaaabaca ; ...
        bool stop = nextValidIndexInPatterns(patterns, i, actionsStr, windowSize) < 0;
        while (!stop)
        {
            try
            {
                if (patterns.Count > 0)
                    i = nextValidIndexInPatterns(patterns, i, actionsStr, windowSize);

                if (i >= 0)
                {

                    string segment = actionsStr.Substring(i, windowSize);
                    int segmentLength = segment.Length;
                    // Gets the occurrences of the segment in the main string. Organizes everything by contiguity.
                    List<List<int>> repeatedOccurrences = AllIndexesOf(actionsStr, segment, true);
                    if (repeatedOccurrences.Sum(r => r.Count) >= minimalOccurences)
                    {
                        // We only want to add indexes that won't intersect existing intervals
                        List<Tuple<int, int>> intervals = PatternIndexIntervals(patterns);

                        foreach (List<int> occurences in repeatedOccurrences)
                        {
                            List<int> filteredIndexes = new List<int>(); // = occurences.Where(idx => !intervals.Exists(interval => IntervalsIntersect(new Tuple<int, int>(idx, idx + segmentLength - 1), interval)));
                            foreach (int idx in occurences)
                            {
                                if (!intervals.Exists(interval => IntervalsIntersect(new Tuple<int, int>(idx, idx + segmentLength - 1), interval)))
                                {
                                    filteredIndexes.Add(idx);
                                }
                            }


                            if (filteredIndexes.Count() >= minimalOccurences)
                            {
                                patterns.Add(new Pattern()
                                {
                                    Actions = segment,
                                    Indexes = filteredIndexes
                                });
                            }
                        }

                        //repeatedOccurrences.ForEach(indexes =>
                        //{
                        //    //Filters the indexes by computing which ones are not occupied yet
                        //    IEnumerable<int> filteredIndexes = indexes.Where(idx => !intervals.Exists(interval => IntervalsIntersect(new Tuple<int, int>(idx, idx + segmentLength - 1), interval)));
                        //    if (filteredIndexes.Count() >= minimalOccurences)
                        //    {
                        //        patterns.Add(new Pattern()
                        //        {
                        //            Actions = segment,
                        //            Indexes = filteredIndexes
                        //        });
                        //    }
                        //});
                    }
                    i++;
                }
                else
                    stop = true;
            }
            catch (ArgumentOutOfRangeException)
            {
                stop = true; //end of string
            }
        }

        GetPatterns(patterns, actionsStr, idealMinWindowSize, --windowSize, minimalOccurences);



    }


    // Todo: delete this method later. just leaving for the comments
    //Normalize organizes a list of actions on patterns
    // ex: aaaabbaaaabbaacc                                  aaabacaaabacaab               aaaabacaaabacaabaaabaca
    // expected result: 2x aaaabb and 1 x aacc               2x aaabac  and 1x aab         1xa 2x aaabac 1x aab 1x aaabaca
    private void Normaalize()
    {

        // Se calhar para resolver este problemas conceptualizamos uma caixa que ao longo de iterações vai mudando de tamanho
        // Na primeira iteração essa caixa terá o tamanho da string. Se todos os elementos forem iguais, então temos um padrão único de repetição
        // teremos aqui  um ciclo dentro de ciclo. No principal diminuimos essa caixa por uma unidade
        // no outro ciclo iremos deslocar a caixa ao longo da string uma unidade de cada vez
        // cada vez que a caixa possuir um valor igual ao imediatamente anterior, guarda um padrão num grupo
        // quando não possui cria um grupo novo
        // apenas são fixados grupos que possua repetição
        // numa ultima iteração, a caixa terá tamanho de uma unidade, então serão fixados grupos de apenas um caractere
        // assim que um pedaço da string pertencer a um padrão, ela não será considerada para o resto

        // isto será mais efeciente quando um padrão não se repetir mais de 3 vezes?

        string actionsStr = this.ToString(); //converts the list of actions to a string
        int actionsLength = actionsStr.Length;
        for (int boxSize = actionsLength; boxSize > 0; boxSize--)
        {
            bool stop = false;
            int currentIndex = 0;
            string previousSegment = string.Empty;
            // DO NOT FORGET TO CONTEMPLATE WHEN A STRING IS ALL EQUAL
            //List<Tuple<string, int>> pattern = new List<Tuple<string, int>>(); //Each tuple saves a substring and a count
            while (!stop) // ex: aaaabacaaabacaabaaabaca  and boxSize = 5 =>  1st |aaaab|acaaabacaabaaabaca ; 2nd a|aaaba|caaabacaabaaabaca ; ...
            {
                try
                {
                    string segment = actionsStr.Substring(currentIndex, boxSize);
                    if (segment == previousSegment)
                    {

                    }

                    currentIndex++;
                }
                catch (System.ArgumentOutOfRangeException) //no more to segment
                {
                    stop = true;
                }

            }
        }




        // 1 - Put in lists of lists: a,a,a,a; b,b; a,a,a,a; b,b; a,a; c,c
        List<List<TileAction>> tileActionsGroups = new List<List<TileAction>>();

        TileAction previousAction = null;
        // Groups elements in Actions sequentially into a list of similar items
        // When a element of a different type is found, creates a new group
        foreach (TileAction tileAction in Actions)
        {
            List<TileAction> taList = null;
            // If previous element is null (we are in first iteration) or previous action is diferent of this one
            // we move to a new group
            if (previousAction == null || tileAction.Action != previousAction.Action)
            {
                taList = new List<TileAction>();
                tileActionsGroups.Add(taList);
            }
            // Else we get the last group
            else
            {
                taList = tileActionsGroups[tileActionsGroups.Count - 1];
            }

            taList.Add(tileAction);
        }


        // 2 - Since we mostly have sequences of walks separated by a turn (that will also include a walk)
        //     we now group
    }
}




///// <summary>
///// This class just saves a level pattern and data on the amount of each blox used
///// A synthesized pattern can be described by sequences of lines. 
///// 
///// Note to self: I thought about marking the end of a line with an L, or U, or S turn, but
/////                 the way we are going to pattern stuff, lines just and with an L turn.
/////                 That is because, for example, a U turn to be done, the character 
/////                 will have to, for example, turn to the left, then walk one step,
/////                 and then turn to the left again. So in fact, two lines will be detected.
/////                 In a sequence of lines, the last termination shall not be considered, because
/////                 it will depend on how the next pattern starts
/////                 
///// 
///// 
///// </summary>
//public class SynthesizedSolution 
//{
//    struct Pattern
//    {

//    }
//}

//public class LineSequence
//{
//    public List<Line> seq { get; set; }
//    public struct Line
//    {
//        public int size;
//        public int endingType; //-1 for left, 0 for none, 1 for right
//    }
//}