﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabManager : MonoBehaviour
{
    [System.Serializable]
    public class TabDescriptor
    {
        [SerializeField] public TabButton TabButton;
        [SerializeField] public RectTransform Panel;

        public bool Active { get; set; }
    }

    [SerializeField] List<TabDescriptor> TabDescriptors;

    RectTransform ScheduledPanel = null;

    // Start is called before the first frame update
    void Start()
    {
        if(TabDescriptors!=null && TabDescriptors.Count  > 0)
        {
            TabDescriptors[0].Active = true;

            foreach (TabDescriptor descriptor in TabDescriptors)
            {
                descriptor.TabButton.SetListener(delegate { ManageClick(descriptor.TabButton); });
            }

        }
         
    }

    // Update is called once per frame
    void Update()
    {
        if (ScheduledPanel != null)
        {
            ForceActive(ScheduledPanel);
            ScheduledPanel = null;
        }

    }

    void ManageClick(TabButton btn)
    {
        ForceActive(btn);
    }


    void ActivateTab(TabDescriptor tab)
    {
        tab.Active = true;
        tab.Panel.gameObject.SetActive(true);
    }

    void InactivateTab(TabDescriptor tab)
    {
        tab.Active = false;
        tab.Panel.gameObject.SetActive(false);
    }

    public void ForceActive(TabButton tabButton)
    {
        TabDescriptor activeTab = TabDescriptors.First(t => t.Active);
        TabDescriptor currentTab = TabDescriptors.First(t => t.TabButton == tabButton);
        InactivateTab(activeTab);
        ActivateTab(currentTab);
    }

    public void ForceActive(RectTransform panel)
    {
        TabDescriptor activeTab = TabDescriptors.First(t => t.Active);
        TabDescriptor currentTab = TabDescriptors.First(t => t.Panel == panel);
        InactivateTab(activeTab);
        ActivateTab(currentTab);
    }
    public void ForceActiveAsync(RectTransform panel)
    {
        ScheduledPanel = panel;
    }

}



