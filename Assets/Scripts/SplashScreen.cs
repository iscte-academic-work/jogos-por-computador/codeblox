﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashScreen : MonoBehaviour
{
    [SerializeField] int SplashScreenDuration = 5;
    DateTime startedAt; 
    // Start is called before the first frame update
    void Start()
    {
        startedAt = DateTime.Now;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("escape") || (DateTime.Now-startedAt).TotalSeconds >= SplashScreenDuration)
        {
            SceneLoader sceneLoader = GetComponent<SceneLoader>();
            sceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.NEXT);
        }
    }
}
