﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsPanel : MonoBehaviour
{
    [SerializeField] SceneLoader SceneLoader;
    static bool SoundFXOn = true;
    static bool MusicOn = true;
    static bool LevelMusicOn = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void SwitchSoundState(string objectsTag, bool state)
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag(objectsTag);
        if (objs != null && objs.Length > 0)
            foreach (GameObject obj in objs)
            {
                try
                {
                    AudioSource audioSource = obj.GetComponent<AudioSource>();
                    audioSource.mute = !state;
                }
                catch (Exception) { }
            }
    }

    public void SwitchLevelMusicState()
    {
        LevelMusicOn = !LevelMusicOn;
        SwitchSoundState("levelmusic", LevelMusicOn); 
    }

    public void SwitchSoundFXState()
    {
        SoundFXOn = !SoundFXOn;
        SwitchSoundState("soundfx", SoundFXOn);
    }

    public void SwitchMusicState()
    {
        SoundFXOn = !SoundFXOn;
        SwitchSoundState("music", SoundFXOn);
    }

    public void GoBack() {
        SceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.GO_BACK);
    }
}
