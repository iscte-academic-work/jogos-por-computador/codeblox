﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// https://answers.unity.com/questions/1253516/playing-audio-through-multiple-scenes.html
/// </summary>
public class NonStopMusic : MonoBehaviour
{
    static List<string> tags = new List<string>();
    [SerializeField] string tag="music";
    [SerializeField] bool unique = true;

    void Awake()
    {
 
        GameObject[] objs = GameObject.FindGameObjectsWithTag(tag);
        if (objs.Length > 1)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        if (!tags.Exists(t => t == tag))
            tags.Add(tag);

        if (unique)
            DestroyAllOffTag();
    }

    //Destroyes every NonStopMusic instance that does not have this tag
    void DestroyAllOffTag()
    {
        foreach(string t in tags)
        {
            if (t != tag)
            {
                GameObject[] objs = GameObject.FindGameObjectsWithTag(t);
                if(objs!=null && objs.Length > 0)
                {
                    foreach (GameObject obj in objs)
                        Destroy(obj);
                }
            }
        }
    }
}