﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NewGame()
    {
        SceneLoader sceneLoader = GetComponent<SceneLoader>();
        sceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.NEW_GAME,1);
    }

    public void LoadGameMenu()
    {
        SceneLoader sceneLoader = GetComponent<SceneLoader>();
        sceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.GO_TO_LOAD_GAME_SCREEN);
    }

    public void LoadCredits()
    {
        SceneLoader sceneLoader = GetComponent<SceneLoader>();
        sceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.LOAD_CREDITS);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
