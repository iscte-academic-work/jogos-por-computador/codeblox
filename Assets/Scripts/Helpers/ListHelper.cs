﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ListHelper  
{
    /// <summary>
    /// Swaps elements
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="list"></param>
    /// <param name="idxA"></param>
    /// <param name="idxB"></param>
    public static void Swap<T>(List<T> list, int idxA, int idxB)
    {
        T aux = list[idxA];
        list[idxA] = list[idxB];
        list[idxB] = aux;
    }

    /// <summary>
    /// Gets a random entry from a list of values. Returns null if list is null or empty
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="entryList"></param>
    /// <returns></returns>
    public static T GetRandomEntry<T>(List<T> entryList)
    {
        return entryList == null || entryList.Count == 0 ? default(T) : entryList[UnityEngine.Random.Range(0, entryList.Count)];
    }
}
