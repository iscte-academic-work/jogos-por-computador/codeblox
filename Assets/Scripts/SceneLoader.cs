﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    public class SceneState
    {
        public SceneDescriber SceneDescriber { get; set; }
        public Dictionary<SceneTransitionEvent, SceneType> SceneEventMap { get; set; }

        public SceneState(SceneDescriber sceneDescriber, Dictionary<SceneTransitionEvent, SceneType> sceneEventMap)
        {
            SceneDescriber = sceneDescriber;
            SceneEventMap = sceneEventMap;
        }
    }

    public class SceneDescriber
    {
        public int? LevelNumber { get; private set; }
        /// <summary>
        /// Possible variations for the level scenes
        /// </summary>
        public List<LevelVariant> LevelVariants { get; private set; }
        /// <summary>
        /// Base name for the Scene
        /// </summary>
        public SceneType BaseName { get; private set; }
        /// <summary>
        /// Current level variant
        /// </summary>
        public LevelVariant? Variant { get; private set; }

        public bool LastLevel { get; }
        /// <summary>
        /// Scene name
        /// </summary>
        public string SceneName
        {
            get
            {
                if (IsLevel && Variant == null) RandomizeLevelSceneName();
                return IsLevel ? BaseName.ToString() + LevelNumber.Value + Variant.Value.ToString() : BaseName.ToString();
            }
        }
        public bool IsLevel { get; private set; }
        public AudioClip BackgroundAudio { get; set; }

        public SceneDescriber(SceneType nonLevelSceneName)
        {
            BaseName = nonLevelSceneName;
        }

        public SceneDescriber(SceneType levelBaseName, List<LevelVariant> levelVariants, int levelNumber, bool last = false)
        {
            LevelVariants = levelVariants;
            BaseName = levelBaseName;
            IsLevel = true;
            LevelNumber = levelNumber;
            LastLevel = last;
        }

        public void RandomizeLevelSceneName()
        {
            if (IsLevel)
            {
                Variant = ListHelper.GetRandomEntry(LevelVariants);
            }
        }


    }

    public enum SceneType
    {
        SplashScreen,
        MainMenu,
        LoadGameMenu,
        Credits,
        Level
    }

    public enum LevelVariant
    {
        A, B, C, D
    }

    public enum SceneTransitionEvent
    {
        NEXT_LEVEL,
        END_GAME,
        GO_BACK,
        NEW_GAME,
        LOAD_LEVEL,
        NEXT,
        GO_TO_LOAD_GAME_SCREEN,
        LOAD_CREDITS
    }

    [SerializeField] SceneType CurrentSceneType;
    [SerializeField] int CurrentLevelNumber = -1;

    public static List<SceneState> SceneStates = LoadScenesInfo();


    public static List<SceneState> LoadScenesInfo()
    {
        List<SceneState> sceneStates = new List<SceneState>();
        // Splash
        SceneDescriber splash = new SceneDescriber(SceneType.SplashScreen);
        Dictionary<SceneTransitionEvent, SceneType> splashMap = new Dictionary<SceneTransitionEvent, SceneType>();
        splashMap.Add(SceneTransitionEvent.NEXT, SceneType.MainMenu);
        sceneStates.Add(new SceneState(splash, splashMap));

        // Credits
        SceneDescriber credits = new SceneDescriber(SceneType.Credits);
        Dictionary<SceneTransitionEvent, SceneType> creditsMap = new Dictionary<SceneTransitionEvent, SceneType>();
        creditsMap.Add(SceneTransitionEvent.NEXT, SceneType.MainMenu);
        sceneStates.Add(new SceneState(credits, creditsMap));


        // Main menu
        SceneDescriber mainMenu = new SceneDescriber(SceneType.MainMenu);
        Dictionary<SceneTransitionEvent, SceneType> mainMenuMap = new Dictionary<SceneTransitionEvent, SceneType>();
        mainMenuMap.Add(SceneTransitionEvent.NEW_GAME, SceneType.Level);
        mainMenuMap.Add(SceneTransitionEvent.GO_TO_LOAD_GAME_SCREEN, SceneType.LoadGameMenu);
        mainMenuMap.Add(SceneTransitionEvent.LOAD_CREDITS, SceneType.Credits);
        sceneStates.Add(new SceneState(mainMenu, mainMenuMap));

        // Load game menu
        SceneDescriber loadGameMenu = new SceneDescriber(SceneType.LoadGameMenu);
        Dictionary<SceneTransitionEvent, SceneType> loadGameMenuMap = new Dictionary<SceneTransitionEvent, SceneType>();
        loadGameMenuMap.Add(SceneTransitionEvent.GO_BACK, SceneType.MainMenu);
        loadGameMenuMap.Add(SceneTransitionEvent.LOAD_LEVEL, SceneType.Level);
        sceneStates.Add(new SceneState(loadGameMenu, loadGameMenuMap));

        // Levels
        int numberOfLevels = 10;
        for (int i = 1; i <= numberOfLevels; i++)
        {
            List<LevelVariant> levelVariants;
            if (i <= 3 || i == 10)
                levelVariants = new List<LevelVariant>() { LevelVariant.A };
            else
                levelVariants = new List<LevelVariant>() { LevelVariant.A, LevelVariant.B, LevelVariant.C };
            SceneDescriber level = new SceneDescriber(SceneType.Level, levelVariants, i, i == numberOfLevels);
            Dictionary<SceneTransitionEvent, SceneType> levelMap = new Dictionary<SceneTransitionEvent, SceneType>();
            levelMap.Add(SceneTransitionEvent.GO_BACK, SceneType.MainMenu);
            levelMap.Add(SceneTransitionEvent.NEXT_LEVEL, SceneType.Level);
            levelMap.Add(SceneTransitionEvent.END_GAME, SceneType.MainMenu);
            sceneStates.Add(new SceneState(level, levelMap));
        }

        return sceneStates;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }




    public void LoadNextScene(SceneTransitionEvent transitionEvent, int? levelNumber = null)
    {
        try
        {
            // Gets current scene
            SceneState currentScene = SceneStates.First(a => a.SceneDescriber.BaseName == CurrentSceneType && (CurrentLevelNumber < 0 || a.SceneDescriber.LevelNumber == CurrentLevelNumber));
            // Gets next scene base name
            SceneType nextSceneBaseName = currentScene.SceneEventMap[transitionEvent];
            // Gets next scene object
            SceneState nextScene = SceneStates.First(s => s.SceneDescriber.BaseName == nextSceneBaseName && s.SceneDescriber.LevelNumber == levelNumber);
            // Generates a random variant
            nextScene.SceneDescriber.RandomizeLevelSceneName();
            // Loads a scene
            SceneManager.LoadScene(nextScene.SceneDescriber.SceneName);
        }
        catch (Exception)
        {
            SceneManager.LoadScene(SceneType.MainMenu.ToString());
        }
    }

    public void LoadNextLevel()
    {
        LoadLevelBeyond(1);
    }
    public void LoadLevelBeyond(int sumLevels = 1)
    {
        try
        {
            SceneState currentScene = SceneStates.First(a => a.SceneDescriber.BaseName == CurrentSceneType && (CurrentLevelNumber < 0 || a.SceneDescriber.LevelNumber == CurrentLevelNumber));
            if (currentScene.SceneDescriber.IsLevel)
            {
                if (sumLevels > 0 && currentScene.SceneDescriber.LastLevel)
                    LoadNextScene(SceneTransitionEvent.GO_BACK);
                else
                    LoadNextScene(SceneTransitionEvent.NEXT_LEVEL, CurrentLevelNumber + sumLevels);
            }
        }
        catch (Exception)
        {
            SceneManager.LoadScene(SceneType.MainMenu.ToString());
        }
    }

    public void ReloadLevel()
    {
        LoadLevelBeyond(0);
    }
}
