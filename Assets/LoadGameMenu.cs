﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadGameMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadLevel(int level)
    {
        SceneLoader sceneLoader = GetComponent<SceneLoader>();
        sceneLoader.LoadNextScene(SceneLoader.SceneTransitionEvent.LOAD_LEVEL, level);
    }
}
